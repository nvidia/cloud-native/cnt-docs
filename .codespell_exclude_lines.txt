# Include whole lines that have codespell-recognized typos.
# This is better than accepting a typo for ask someplace random.
# End the file with a blank line.
Approaches for Working with Azure AKS
You can approach running workloads in Azure AKS with NVIDIA GPUs in at least two ways.
Default AKS configuration without the GPU Operator
By default, you can run Azure AKS images on GPU-enabled virtual machines with NVIDIA GPUs,
AKS images include a preinstalled NVIDIA GPU Driver and preinstalled NVIDIA Container Toolkit.
`Use GPUs for compute-intensive workloads on Azure Kubernetes Services <https://learn.microsoft.com/en-us/azure/aks/gpu-cluster>`_
The images that are available in AKS always include a preinstalled NVIDIA GPU driver
After you start your Azure AKS cluster, you are ready to install the NVIDIA GPU Operator.
   GPU Operator with Azure AKS <microsoft-aks.rst>
* Added support for running the Operator with Microsoft Azure Kubernetes Service (AKS).
  You must use an AKS image with a preinstalled NVIDIA GPU driver and a preinstalled
